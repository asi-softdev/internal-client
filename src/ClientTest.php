<?php 
namespace AsiAsiapac\InternalClient;

use AsiAsiapac\InternalClient\Component;

class ClientTest extends Component{
	
	public function list_test($client_id = '')
	{
		return $this->_execute('GET', 'client-test/client/'.$client_id);
	}

	public function detail($id)
	{
		return $this->_execute('GET', 'client-test/'.$id);
	}

	/*
		*format data array

		[
			'id_client' => 
	        'id_test' => 
	        'seq_num' => 

		]
	*/
	public function create($data)
	{
		return $this->_execute('POST', 'client-test/', $data);
	}

	/*
		*format data array
		
		[
	        'id_test' => 
	        'seq_num' => 

		]
	*/
	public function update($id, $data)
	{
		return $this->_execute('PUT', 'client-test/'.$id, $data);
	}

	public function delete($id)
	{
		return $this->_execute('DELETE', 'client-test/'.$id);
	}
}
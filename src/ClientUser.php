<?php 
namespace AsiAsiapac\InternalClient;

use AsiAsiapac\InternalClient\Component;

class ClientUser extends Component{
	
	public function list_user($client_id = '')
	{
		return $this->_execute('GET', 'client-user/client/'.$client_id);
	}

	public function detail($id)
	{
		return $this->_execute('GET', 'client-user/'.$id);
	}

	public function create($data, $photo = [])
	{
		$multipart = false;
		if(!empty($photo)){

			$data = $this->_generateFileData($data, [
				'photo' => [
					'name' => $photo['file_name'],
					'file_path' => $photo['file_path'],
				]
			]);

			$multipart = true;
		}

		return $this->_execute('POST', 'client-user/', $data, false, $multipart);
	}

	public function update($id, $data, $photo = [])
	{
		$multipart = false;
		if(!empty($photo)){

			$data = $this->_generateFileData($data, [
				'photo' => [
					'name' => $photo['file_name'],
					'file_path' => $photo['file_path'],
				]
			]);

			$multipart = true;
		}

		return $this->_execute('PUT', 'client-user/'.$id, $data, false, $multipart);
	}

	public function delete($id)
	{
		return $this->_execute('DELETE', 'client-user/'.$id);
	}
}
<?php 
namespace AsiAsiapac\InternalClient;

use AsiAsiapac\InternalClient\Component;

class ClientReport extends Component{
	
	public function list_report($client_id = '')
	{
		return $this->_execute('GET', 'client-report/client/'.$client_id);
	}

	public function detail($id)
	{
		return $this->_execute('GET', 'client-report/'.$id);
	}

	/*
		*format data array

		[
			'id_client' => 
            'group_report_code' => 
            'report_code' => 
            'report_type' => 
            'sequence' => 
            'sub_sequence' => 
            'status' => 
            'languages' => 
            'language_status => 

		]
	*/
	public function create($data)
	{
		return $this->_execute('POST', 'client-report/', $data);
	}

	/*
		*format data array
		
		[
	        'group_report_code' =>
            'report_code' =>
            'report_type' =>
            'sequence' =>
            'sub_sequence' =>
            'status' =>
            'languages =>
            'language_status' =>

		]
	*/
	public function update($id, $data)
	{
		return $this->_execute('PUT', 'client-report/'.$id, $data);
	}

	public function delete($id)
	{
		return $this->_execute('DELETE', 'client-report/'.$id);
	}
}
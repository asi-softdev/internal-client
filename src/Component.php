<?php
namespace AsiAsiapac\InternalClient;

use AsiAsiapac\InternalLogger\SysLogger;
use GuzzleHttp\Client;

class Component
{
    protected $client;

    function __construct()
    {
        $this->client = new Client(['base_uri' => $_ENV['ASI_CLIENT_HOST_ADDR'] ]);
    }

    /*
        execute the api and get the response
    */
    protected function _execute($METHOD, $params, $data = [], $full_path = false, $multipart = false)
    {

        $message = '';
        $error = [];

        $trace = debug_backtrace();

        $back_func  = $trace[1]['function'];
        $class      = $trace[1]['class'];

        $back_func_2  = $trace[2]['function'];
        $class_2      = $trace[2]['class'];

        $class_refer = sprintf('%s\%s -> %s\%s', $class_2, $back_func_2, $class, $back_func);

        try {
            if($full_path){
                $client = new Client(['base_uri' => $params]);

                if(in_array($METHOD, ['GET', 'DELETE'])){
                    $response = $client->request($METHOD, '');
                }else if(in_array($METHOD, ['POST', 'PATCH', 'PUT'])){
                    $response = $client->request($METHOD, '', ['form_params' => $data]);
                }
            }else{
                // $client = $this->client;
                $client = new Client(['base_uri' => $_ENV['ASI_CLIENT_HOST_ADDR'] ]);

                if(in_array($METHOD, ['GET', 'DELETE'])){
                    $response = $client->request($METHOD, $params);
                }else if(in_array($METHOD, ['POST', 'PATCH', 'PUT'])){
                    $head_opt = [
                        'headers' => [
                            'Accept' => 'application/json',
                        ]
                    ];

                    if(!empty($multipart)){
                        $head_opt['multipart'] = $data;
                    }else{
                        $head_opt['form_params'] = $data;
                    }

                    $response = $client->request($METHOD, $params, $head_opt);
                }
            }

            $statusCode = $response->getStatusCode();
            if($statusCode == 200){
                try {
                    $contents = $response->getBody()->getContents();

                    $responseContent = json_decode($contents, true);
                } catch (Exception $e) {
                    $message = $e->getMessage();

                    SysLogger::save(SysLogger::ERROR, $_SERVER['REQUEST_URI'], $class_refer, 'problem with api function, params : '.$params.' - '.$message);
                }
            }else{
                $responseContent = '';
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();

            $responseContent    = $response->getBody()->getContents();
            $statusCode         = $response->getStatusCode();

            if($statusCode == 404){
                $responseContent = 'not found';
            }else if($statusCode != 422 ){
                SysLogger::save(SysLogger::ERROR, $_SERVER['REQUEST_URI'], $class_refer, 'problem with api function callback, params : '.$params.' - '.$responseContent);
            }else{
                $responseContent = json_decode($responseContent);
            }
        }

        return [
            'status'   => $statusCode,
            'message'  => $message,
            'response' => $responseContent
        ];
    }

    protected function _generateFileData($data, $file_data)
    {
        $temp = [];

        foreach ($data as $field => $value) {
            $temp[] = [
                'name' => $field,
                'contents' => $value
            ];
        }

        if(!empty($file_data)){
            foreach ($file_data as $field => $value) {
                $temp[] = [
                    'Content-type' => 'multipart/form-data',
                    'name' => $field,
                    'filename' => $value['name'],
                    'contents' => fopen($value['file_path'], 'r')
                ];
            }
        }

        return $temp;
    }
}
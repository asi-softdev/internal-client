<?php 
namespace AsiAsiapac\InternalClient;

use AsiAsiapac\InternalClient\Component;

class Client extends Component{
	
	public function credit($client_id, $data = [])
	{
		return $this->_execute('PATCH', 'client/credit/'.$client_id, $data);
	}

	public function detail($client_id)
	{
		return $this->_execute('GET', 'client/'.$client_id);
	}

	public function list()
	{
		return $this->_execute('GET', 'client/');
	}

	/*
		*format data array

		[
			'client_name' => 
			'is_project' => 
			'self_capture' => 
			'proctoring' => 
			'olinterview' => 
			'assessment_center' => 
			'client_prefix' => 
			'client_dummy' => 
			'max_client_sequence' => 
			'max_cnd_group' => 
			'min_bandwidth' => 
			'with_uploader' => 
			'bw_check_individual' => 
			'default_test_individual' => 
			'bw_check_group' => 
			'default_test_group' => 
			'credit_total' => 
			'credit_available' => 
			'credit_proctoring_total' => 
			'credit_proctoring_available' => 
			'credit_olinterview_total' => 
			'credit_olinterview_available' => 
			'address' => 
			'postcode' => 
			'phone_number' => 
			'city' => 
			'province' => 
			'country' => 
			'cp_name' => 
			'cp_email' => 
			'cp_phone_number' => 
			'client_logo' => 
		]

		format data client_logo

		[
			file_name => 
			file_path => 
		]
	*/
	public function create($data, $client_logo = [])
	{

		$multipart = false;
		if(!empty($client_logo)){

			$data = $this->_generateFileData($data, [
				'client_logo' => [
					'name' => $client_logo['file_name'],
					'file_path' => $client_logo['file_path'],
				]
			]);

			$multipart = true;
		}

		return $this->_execute('POST', 'client/', $data, false, $multipart);
	}

	/*
		*format data array

		[
			'client_name' => 
			'is_project' => 
			'self_capture' => 
			'proctoring' => 
			'olinterview' => 
			'assessment_center' => 
			'client_prefix' => 
			'client_dummy' => 
			'max_client_sequence' => 
			'max_cnd_group' => 
			'min_bandwidth' => 
			'with_uploader' => 
			'bw_check_individual' => 
			'default_test_individual' => 
			'bw_check_group' => 
			'default_test_group' => 
			'credit_total' => 
			'credit_available' => 
			'credit_proctoring_total' => 
			'credit_proctoring_available' => 
			'credit_olinterview_total' => 
			'credit_olinterview_available' => 
			'address' => 
			'postcode' => 
			'phone_number' => 
			'city' => 
			'province' => 
			'country' => 
			'cp_name' => 
			'cp_email' => 
			'cp_phone_number' => 
			'client_logo' => 
		]

		format data client_logo

		[
			file_name => 
			file_path => 
		]
	*/
	public function update($id, $data, $client_logo = [])
	{
		$multipart = false;
		if(!empty($client_logo)){

			$data = $this->_generateFileData($data, [
				'client_logo' => [
					'name' => $client_logo['file_name'],
					'file_path' => $client_logo['file_path'],
				]
			]);

			$multipart = true;
		}

		return $this->_execute('PUT', 'client/'.$id, $data);
	}

	public function delete($id)
	{
		return $this->_execute('DELETE', 'client/'.$id);
	}
}